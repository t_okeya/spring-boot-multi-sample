package jp.co.sample.api.security;

import jp.co.sample.shared.util.LogUtil;
import org.springframework.security.web.header.HeaderWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AppAuthenticationHeaderWriter.
 *
 * @author t.okeya
 */
public class AppAuthenticationHeaderWriter implements HeaderWriter {

    /** {@inheritDoc} */
    @Override
    public void writeHeaders(HttpServletRequest request, HttpServletResponse response) {

        LogUtil.debug("writeHeaders");

        // レスポンスに共通で出力したいものがあれば記載する
    }
}
