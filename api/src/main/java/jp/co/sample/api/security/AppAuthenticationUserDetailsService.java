package jp.co.sample.api.security;

import jp.co.sample.shared.domain.entity.Role;
import jp.co.sample.shared.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import jp.co.sample.shared.domain.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * AppAuthenticationUserDetailsService.
 *
 * @author t.okeya
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class AppAuthenticationUserDetailsService implements
        AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    /** . */
    private String[] nonChecks;

    /** {@link UserRepository}. */
    @Autowired
    private UserRepository userRepository;

    /**
     * コンストラクタです.
     * @param nonChecks {@link String[]}
     */
    public AppAuthenticationUserDetailsService(String[] nonChecks) {
        this.nonChecks = nonChecks;
    }

    /** {@inheritDoc} */
    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {

        AppPreAuthentication principal = (AppPreAuthentication) token.getPrincipal();

        String username = "anonymous";
        User user = new User();
        Set<Role> roles = new HashSet<>();

        if (!StringUtils.isEmpty(principal.getToken())) {
            // トークンがあれば

            if (this.isTempAuthToken(principal.getPath())) {
                // 仮トークンを使用するAPIの場合

                // トークンを元に、仮ユーザ情報を取得する処理をここに記載する
                username = principal.getToken();

                user = this.userRepository.findByLogin(username).orElse(new User());
            } else {
                // 本トークンを使用するAPIの場合

                // トークンを元に、本ユーザ情報を取得する処理をここに記載する
                username = principal.getToken();

                user = this.userRepository.findByLogin(username).orElse(new User());
            }
        }

        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), authorities(roles));
    }

    /**
     * @param resourcePath {@link String}
     * @return {@link Boolean}
     */
    private boolean isTempAuthToken(String resourcePath) {
        for (String s : nonChecks) {
            if (resourcePath.contains(s)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param roles {@link Set <Role>}.
     * @return {@link List <AppGrantedAuthority>}.
     */
    private static List<AppGrantedAuthority> authorities(Set<Role> roles) {
        List<AppGrantedAuthority> authorities = new ArrayList<>();
        roles.forEach(role ->
                role.getPermissions().forEach(permission ->
                        authorities.add(new AppGrantedAuthority(permission.getName(), permission.getName()))));
        return authorities;
    }

}
