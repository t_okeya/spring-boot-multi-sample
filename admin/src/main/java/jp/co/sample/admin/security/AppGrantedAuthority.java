package jp.co.sample.admin.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * AppGrantedAuthority.
 *
 * @author t.okeya
 */
@AllArgsConstructor
@Data
public class AppGrantedAuthority implements GrantedAuthority {

    /** 権限名. */
    private final String name;
    /** 権限コード. */
    private final String code;

    /** {@inheritDoc} */
    @Override
    public String getAuthority() {
        return this.code;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof AppGrantedAuthority) {
            return this.code.equals(((AppGrantedAuthority) obj).code);
        }
        return false;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return this.code.hashCode();
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return this.code;
    }}
