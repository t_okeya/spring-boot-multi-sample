package jp.co.sample.shared.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * 権限テーブル.
 *
 * @author t.okeya
 */
@Entity
@Getter
@Setter
@Table(name = "permission")
public class Permission extends AbstractEntity {

    // Column

    /**
     * ID.
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 権限名.
     */
    @Column(name = "name")
    private String name;

    /**
     * 説明.
     */
    @Column(name = "description")
    private String description;


    // Relation


    // Method

}
