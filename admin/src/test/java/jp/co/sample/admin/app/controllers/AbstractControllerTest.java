package jp.co.sample.admin.app.controllers;

import jp.co.sample.admin.security.AppAuthentication;
import jp.co.sample.admin.security.AppAuthenticationUser;
import jp.co.sample.shared.domain.entity.Permission;
import jp.co.sample.shared.domain.entity.Role;
import jp.co.sample.shared.domain.entity.User;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.*;

/**
 * AbstractControllerTest.
 *
 * @author t.okeya
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractControllerTest {

    /** {@link SecurityContext}. */
    public SecurityContext securityContext;

    /** {@link MockMvc}. */
    public MockMvc mockMvc;

    /** モック化対象インタフェース {@link AppAuthentication}. */
    @Mock
    private AppAuthentication appAuthentication;

    /**
     * 前処理.
     * @param target .
     */
    protected void before(AbstractController target) {
        mockMvc = MockMvcBuilders
                .standaloneSetup(target)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .setViewResolvers(new ViewResolver() {
                    @Override
                    public View resolveViewName(String viewName, Locale locale) throws Exception {
                        return new MappingJackson2JsonView();
                    }
                })
                .build();
    }

    /**
     * 後処理.
     */
    protected void after() {
        SecurityContextHolder.clearContext();
    }

    /** ロール権限. */
    private final static List<Integer[]> role_permission = new LinkedList<>();
    static {
        role_permission.add(new Integer[]{1, 2});
        role_permission.add(new Integer[]{2});
    }

    /** ロール. */
    private final static List<String[]> roles = new LinkedList<>();
    static {
        roles.add(new String[]{"1", "ROLE_ADMIN", "1", "管理ユーザ権限"});
        roles.add(new String[]{"2", "ROLE_USER", "2", "一般ユーザ権限"});
    }

    /** 権限. */
    private final static List<String[]> permissions = new LinkedList<>();
    static {
        permissions.add(new String[]{"1", "ユーザ情報更新権限", ""});
        permissions.add(new String[]{"2", "ユーザ情報参照権限", ""});
    }

    /**
     * 認証処理(システム管理者).
     *
     * @throws Exception .
     */
    protected void authentication() throws Exception {
        // システム管理者固定
        this.authentication("admin_user", "admin_pass", 1);
    }

    /**
     * 認証処理(システム管理者以外).
     *
     * @param loginId .
     * @param password .
     * @param roleId .
     * @throws Exception .
     */
    protected void authentication(String loginId, String password, int roleId) throws Exception {
        this.setAuthentication(loginId, password, roleId);
        Mockito.when(appAuthentication.user()).thenReturn(this.authUser(loginId, password, roleId));
    }

    /**
     * コンテキストに認証情報を設定します.
     *
     * @param loginId .
     * @param password .
     * @param roleId .
     * @throws Exception .
     */
    private void setAuthentication(@NonNull String loginId,
                                   @NonNull String password,
                                   @NonNull int roleId) throws Exception {
        AppAuthenticationUser authUser = this.authUser(loginId, password, roleId)
                .orElseThrow(Exception::new);
        Authentication authentication
                = new UsernamePasswordAuthenticationToken(authUser, null, authUser.getAuthorities());
        securityContext = SecurityContextHolder.createEmptyContext();
        securityContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    /**
     * 認証情報を返却します.
     *
     * @param loginId .
     * @param password .
     * @param roleId .
     */
    private Optional<AppAuthenticationUser> authUser(@NonNull String loginId,
                                                     @NonNull String password,
                                                     @NonNull int roleId) {
        User user = this.user(loginId, password, roleId);
        Set<Role> roles = Optional.ofNullable(user.getRoles()).orElse(new HashSet<>());
        return Optional.of(new AppAuthenticationUser(user, roles));
    }

    /**
     * 管理ユーザ情報を返却します.
     *
     * @param loginId .
     * @param password .
     * @param roleId .
     * @return .
     */
    private User user(String loginId, String password, int roleId) {
        User user = new User();
        user.setLogin(loginId);
        user.setPassword(password);

        HashSet<Role> roles = new HashSet<>();
        roles.add(this.role(roleId));
        user.setRoles(roles);
        return user;
    }

    /**
     * ロール情報を返却します.
     *
     * @param id .
     * @return .
     */
    private Role role(int id) {
        String[] attr = roles.get(id - 1);
        Role role = new Role();
        role.setId(Long.valueOf(attr[0]));
        role.setName(attr[1]);
        role.setDisplayOrder(attr[2]);
        role.setDescription(attr[3]);

        Integer[] rolePermission = role_permission.get(id - 1);
        for (Integer permissionId : rolePermission) {
            HashSet<Permission> permissions = new HashSet<>();
            permissions.add(this.permission(permissionId));
            role.setPermissions(permissions);
        }
        return role;
    }

    /**
     * 権限情報を返却します.
     *
     * @param id .
     * @return .
     */
    private Permission permission(int id) {
        String[] attr = permissions.get(id - 1);
        Permission permission = new Permission();
        permission.setId(Long.valueOf(attr[0]));
        permission.setName(attr[1]);
        permission.setDescription(attr[2]);
        return permission;
    }
}