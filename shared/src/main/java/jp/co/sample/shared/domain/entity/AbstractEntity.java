package jp.co.sample.shared.domain.entity;

import java.io.Serializable;

/**
 * <code>Entity</code>の基底クラス.
 *
 * @author t.okeya
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractEntity implements Serializable {


}
