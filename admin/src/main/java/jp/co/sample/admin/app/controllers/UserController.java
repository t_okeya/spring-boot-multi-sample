package jp.co.sample.admin.app.controllers;

import jp.co.sample.admin.app.forms.user.SearchForm;
import jp.co.sample.admin.app.forms.user.StoreForm;
import jp.co.sample.admin.app.forms.user.UpdateForm;
import jp.co.sample.admin.domain.service.UserService;
import jp.co.sample.shared.domain.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/user")
@SuppressWarnings("unused")
public class UserController extends AbstractController {

    /** {@link UserService}. */
    @Autowired
    private UserService userService;

    /**
     * show list page.
     *
     * @param pageable {@link Pageable}
     * @param form {@link SearchForm}
     * @param result {@link BindingResult}
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public ModelAndView index(@PageableDefault(value = PAGE_SIZE) Pageable pageable,
                              @ModelAttribute("searchForm") @Validated SearchForm form,
                              BindingResult result,
                              ModelAndView mv) {
        Page<User> users = this.userService.findAll(form, pageable);
        mv.addObject("users", users);
        return response(mv, "user/index");
    }

    /**
     * show register page.
     *
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = {"/create"}, method = RequestMethod.GET)
    public ModelAndView create(@ModelAttribute("storeForm") StoreForm form,
                               ModelAndView mv) {
        return response(mv, "user/create");
    }

    /**
     * processing register.
     *
     * @param form {@link StoreForm}
     * @param result {@link BindingResult}
     * @param attributes {@link RedirectAttributes}
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = {"/"}, method = RequestMethod.POST)
    public ModelAndView store(@ModelAttribute("storeForm") @Validated StoreForm form,
                              BindingResult result,
                              RedirectAttributes attributes,
                              ModelAndView mv) {

        if (result.hasErrors()) {
            return this.create(form, mv);
        }

        // write on create process

        attributes.addFlashAttribute("infoMessage", "store success.");
        return redirect(mv, "/user/");
    }

    /**
     * show detail page.
     *
     * @param id {@link String}
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     * @throws Exception .
     */
    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ModelAndView show(@PathVariable String id,
                             ModelAndView mv) throws Exception {
        User user = this.userService.findById(Long.valueOf(id)).orElseThrow(Exception::new);
        mv.addObject("user", user);
        return response(mv, "user/show");
    }

    /**
     * show edit page.
     *
     * @param id {@link String}
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     * @throws Exception .
     */
    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable String id,
                             @ModelAttribute("updateForm") UpdateForm form,
                             ModelAndView mv) throws Exception {

        User user = this.userService.findById(Long.valueOf(id)).orElseThrow(Exception::new);
        populate(form, user);
        mv.addObject("user", user);
        return response(mv, "user/edit");
    }

    /**
     * processing update.
     *
     * @param id {@link String}
     * @param form {@link UpdateForm}
     * @param result {@link BindingResult}
     * @param attributes {@link RedirectAttributes}
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     * @throws Exception .
     */
    @RequestMapping(value = {"/{id}"}, method = RequestMethod.POST)
    public ModelAndView update(@PathVariable String id,
                               @ModelAttribute("updateForm") @Validated UpdateForm form,
                               BindingResult result,
                               RedirectAttributes attributes,
                               ModelAndView mv) throws Exception {

        if (result.hasErrors()) {
            return this.edit(id, form, mv);
        }

        // write on update process

        attributes.addFlashAttribute("infoMessage", "update success.");
        return redirect(mv, "/user/" + id);
    }

}
