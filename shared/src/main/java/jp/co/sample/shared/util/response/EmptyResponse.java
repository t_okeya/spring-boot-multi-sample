package jp.co.sample.shared.util.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * EmptyResponse.
 *
 * @author t.okeya
 */
@JsonSerialize
public class EmptyResponse {
}
