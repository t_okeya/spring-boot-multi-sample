package jp.co.sample.admin.app.controllers;

import jp.co.sample.admin.domain.service.UserService;
import jp.co.sample.shared.domain.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@SuppressWarnings("WeakerAccess")
public class UserControllerTest extends AbstractControllerTest {

    /** 対象コントローラ {@link UserController} */
    @InjectMocks
    private UserController target;

    /** モック化対象サービス {@link UserService} */
    @Mock
    private UserService userService;

    /**
     * 前処理.
     */
    @Before
    public void before() {
        super.before(target);
        MockitoAnnotations.initMocks(this);
    }

    /**
     * 後処理.
     */
    @After
    @Override
    public void after() {
        super.after();
    }

    /**
     * ユーザ一覧(index)テスト.
     *
     * @throws Exception .
     */
    @Test
    public void testCase1() throws Exception {

        // システム管理者にて認証
        authentication();

        // findAllの戻り値モック化(0件)
        List<User> list = new ArrayList<>();
        Page<User> users = new PageImpl<>(list);

        Mockito
                .doReturn(users)
                .when(this.userService)
                .findAll(Mockito.anyObject(), Mockito.anyObject());

        // テスト実施
        mockMvc
                // URL指定
                .perform(MockMvcRequestBuilders.get("/user/"))
                // HTTPステータスの確認
                .andExpect(MockMvcResultMatchers.status().isOk())
                // 指定テンプレートの確認
                .andExpect(MockMvcResultMatchers.view().name("user/index"))
                // attributeに設定した内容の確認
                .andExpect(MockMvcResultMatchers.model().attribute("users", users))
        ;
    }
}