package jp.co.sample.shared.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * ロールテーブル.
 *
 * @author t.okeya
 */
@Entity
@Getter
@Setter
@Table(name = "role")
public class Role extends AbstractEntity {

    // Column

    /**
     * ID.
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * ロール名.
     */
    @Column(name = "name")
    private String name;

    /**
     * 表示順.
     */
    @Column(name = "display_order")
    private String displayOrder;

    /**
     * 説明.
     */
    @Column(name = "description")
    private String description;


    // Relation

    /**
     * permissions {@link List<Permission>}
     */
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "role_permission",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    private Set<Permission> permissions;


    // Method

}
