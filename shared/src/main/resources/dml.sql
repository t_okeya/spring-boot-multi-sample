-- company
INSERT INTO `company` (`id`, `code`, `name`, `name_kana`, `post_code`, `prefecture`, `address1`, `address2`, `tel`, `mail`, `deleted_at`, `created_user_id`, `created_at`, `updated_user_id`, `updated_at`)
VALUES
	(1, 'C00001', '企業１', 'キギョウ１', '1234567', '東京都', '住所１', '住所２', '03-1234−5678', 'campany1@test.co.jp', NULL, NULL, now(), NULL, NULL),
	(2, 'C00002', '企業２', 'キギョウ２', '123-4567', '大阪府', '住所１', '住所２', '075-123-4567', 'campany1@test.co.jp', NULL, NULL, now(), NULL, NULL);

-- user
INSERT INTO `user` (`id`, `company_id`, `code`, `login`, `password`, `name`, `mail`, `last_password_reset_date`, `deleted_at`, `created_user_id`, `created_at`, `updated_user_id`, `updated_at`)
VALUES
	(1, 1, 'A00001', 'admin', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', '管理者', 'admin@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(2, 1, 'U00001', 'user1', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ1', 'user1@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(3, 1, 'U00002', 'user2', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ2', 'user2@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(4, 1, 'U00003', 'user3', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ3', 'user3@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(5, 1, 'U00004', 'user4', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ4', 'user4@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(6, 1, 'U00005', 'user5', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ5', 'user5@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(7, 1, 'U00006', 'user6', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ6', 'user6@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(8, 1, 'U00007', 'user7', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ7', 'user7@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(9, 1, 'U00008', 'user8', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ8', 'user8@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(10, 1, 'U00009', 'user9', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ9', 'user9@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(11, 1, 'U00010', 'user10', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ10', 'user10@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(12, 1, 'U00011', 'user11', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ11', 'user11@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(13, 1, 'U00012', 'user12', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ12', 'user12@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(14, 1, 'U00013', 'user13', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ13', 'user13@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(15, 1, 'U00014', 'user14', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ14', 'user14@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(16, 1, 'U00015', 'user15', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ15', 'user15@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(17, 1, 'U00016', 'user16', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ16', 'user16@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL),
	(18, 1, 'U00017', 'user17', '$2a$10$rxpI4b.lFsk2x2fkipStkuO3eNV0bQIPWdYoqirSCnrP5nkKxHXmu', 'ユーザ17', 'user17@test.co.jp', NULL, NULL, NULL, '2018-05-05 11:28:28', NULL, NULL);

-- role
INSERT INTO `role` (`id`, `name`, `display_order`, `description`)
VALUES
	(1, 'ROLE_ADMIN', 1, '管理ユーザ権限'),
	(2, 'ROLE_USER', 2,  '一般ユーザ権限');

-- user_role
INSERT INTO `user_role` (`user_id`, `role_id`)
VALUES
	(1, 1),
	(1, 2),
	(2, 2);

-- permission
INSERT INTO `permission` (`id`, `name`, `description`)
VALUES
	(1, 'ユーザ情報更新権限', NULL),
	(2, 'ユーザ情報参照権限', NULL);

-- role_permission
INSERT INTO `role_permission` (`permission_id`, `role_id`)
VALUES
	(1, 1),
	(1, 2),
	(2, 2);
