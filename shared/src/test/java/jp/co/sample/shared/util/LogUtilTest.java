package jp.co.sample.shared.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import jp.co.sample.shared.config.LogConfiguration;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LogUtilTest.
 *
 * @author t.okeya
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class LogUtilTest {

    /** Appender. */
    @Mock
    private Appender mockAppender;
    /** ArgumentCaptor. */
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    /** @return {@link LogConfigTest} */
    @Bean
    public LogConfigTest logConfigTest() {
        return new LogConfigTest();
    }

    /**
     * LogConfigTest.
     */
    public class LogConfigTest implements LogConfiguration {
        /** {@inheritDoc} */
        @Override
        public Map<String, String> getMessages() {
            Map<String, String> messages = new LinkedHashMap<>();
            messages.put("001-00001", "message");
            messages.put("001-00002", "message {}");
            messages.put("001-00003", "message {} {}");
            return messages;
        }
        /** {@inheritDoc} */
        @Override
        public String message(String code) {
            return this.getMessages().getOrDefault(code, "対象のメッセージコードが存在しません。");
        }
    }

    /**
     * setUp.
     */
    @Before
    public void setUp() {
        final Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(this.mockAppender);
        LogUtil.setLogConfiguration(this.logConfigTest());
    }

    /**
     * tearDown.
     */
    @After
    public void tearDown() {
        final Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.detachAppender(this.mockAppender);
    }

    /**
     * debug.
     */
    @Test
    public void case1() {
        LogUtil.debug("message");
        this.execute(Level.DEBUG, "message");
    }

    /**
     * debug.
     */
    @Test
    public void case2() {
        LogUtil.debug("message {}", "user_id");
        this.execute(Level.DEBUG, "message user_id");
    }

    /**
     * info.
     */
    @Test
    public void case3() {
        LogUtil.info("000-00000");
        this.execute(Level.INFO, "000-00000 対象のメッセージコードが存在しません。");
    }

    /**
     * info.
     */
    @Test
    public void case4() {
        LogUtil.info("001-00001");
        this.execute(Level.INFO, "001-00001 message");
    }

    /**
     * info.
     */
    @Test
    public void case5() {
        LogUtil.info("001-00002", "user_id");
        this.execute(Level.INFO, "001-00002 message user_id");
    }

    /**
     * warn.
     */
    @Test
    public void case6() {
        LogUtil.warn("001-00001");
        this.execute(Level.WARN, "001-00001 message");
    }

    /**
     * warn.
     */
    @Test
    public void case7() {
        LogUtil.warn("001-00003", "user_id", "wallet_service_id");
        this.execute(Level.WARN, "001-00003 message user_id wallet_service_id");
    }

    /**
     * error.
     */
    @Test
    public void case8() {
        LogUtil.error("001-00001");
        this.execute(Level.ERROR, "001-00001 message");
    }

    /**
     * error.
     */
    @Test
    public void case9() {
        LogUtil.error("001-00003", "user_id", "wallet_service_id");
        this.execute(Level.ERROR, "001-00003 message user_id wallet_service_id");
    }

    /**
     * ログ出力内容を比較します.
     *
     * @param level ログレベル
     * @param message 出力メッセージ
     */
    private void execute(Level level, String message) {
        Mockito.verify(this.mockAppender).doAppend(this.captorLoggingEvent.capture());
        final LoggingEvent loggingEvent = this.captorLoggingEvent.getValue();
        Assert.assertThat(loggingEvent.getLevel(), CoreMatchers.is(level));
        Assert.assertThat(loggingEvent.getFormattedMessage(), CoreMatchers.is(message));
    }

}