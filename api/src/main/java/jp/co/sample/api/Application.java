package jp.co.sample.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Application.
 *
 * @author t.okeya
 */
@EnableAutoConfiguration
//@EnableScheduling
@EntityScan("jp.co.sample.shared.domain.*")
@EnableJpaRepositories("jp.co.sample.shared.domain.*")
@SpringBootApplication(scanBasePackages = {"jp.co.sample.*"})
public class Application extends SpringBootServletInitializer {

    /**
     * @param args {@link String[]}
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /** {@inheritDoc} */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}
