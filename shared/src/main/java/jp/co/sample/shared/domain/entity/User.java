package jp.co.sample.shared.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

/**
 * 管理ユーザテーブル.
 *
 * @author t.okeya
 */
@Entity
@Getter
@Setter
@Table(name = "user")
@Where(clause = "deleted_at IS NULL") // 論理削除されてない条件
public class User extends AbstractEntity {

    // Column

    /**
     * ID
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 企業ID.
     */
    @Column(name = "company_id", nullable = false)
    private String companyId;

    /**
     * ユーザコード.
     */
    @Column(name = "code")
    private String code;

    /**
     * ログインID.
     */
    @Column(name = "login", nullable = false)
    private String login;

    /**
     * パスワード.
     */
    @JsonIgnore
    @Column(name = "password", nullable = false)
    private String password;

    /**
     * 氏名.
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * メールアドレス.
     */
    @Column(name = "mail")
    private String mail;

    /**
     * 最終パスワード設定日.
     */
    @Column(name = "last_password_reset_date")
    private Timestamp lastPasswordResetDate;

    /**
     * 削除日時.
     */
    @Column(name = "deleted_at")
    private Timestamp deletedAt;

    /**
     * 作成ユーザID.
     */
    @Column(name = "created_user_id", insertable = false, updatable = false)
    private Long createdUserId;

    /**
     * 作成日時.
     */
    @Column(name = "created_at", insertable = false, updatable = false)
    private Timestamp createdAt;

    /**
     * 最終更新ユーザID.
     */
    @Column(name = "updated_user_id", insertable = false, updatable = false)
    private Long updatedUserId;

    /**
     * 最終更新日時.
     */
    @Column(name = "updated_at", insertable = false, updatable = false)
    private Timestamp updatedAt;


    // Relation

    /**
     * roles {@link Set<Role>}
     */
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, insertable = false, updatable = false, name = "company_id")
    private Company company;


    // Method


}
