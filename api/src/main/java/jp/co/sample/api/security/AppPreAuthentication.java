package jp.co.sample.api.security;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * PreAuthenticatedProcessingFilter - AuthenticationUserDetailsService
 * 間でやりとりするためのモデルクラス.
 *
 * @author t.okeya
 */
@AllArgsConstructor
@Data
@SuppressWarnings("WeakerAccess")
public class AppPreAuthentication {

    /** token. */
    private String token;
    /** path. */
    private String path;
}
