package jp.co.sample.admin.security;

import jp.co.sample.shared.domain.entity.Role;
import jp.co.sample.shared.domain.entity.User;
import jp.co.sample.shared.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * UserDetailsService実処理.
 *
 * @author t.okeya
 */
@Service("userDetailsService")
@SuppressWarnings("unused")
public class AppUserDetailsService implements UserDetailsService {

    /** {@link UserRepository}. */
    @Autowired
    private UserRepository userRepository;

    /** {@inheritDoc} */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if (username == null || username.isEmpty()) {
            throw new UsernameNotFoundException("Username is empty");
        }

        User user = this.userRepository.findByLogin(username).orElseThrow(()
                -> new UsernameNotFoundException(String.format("User not found \"%s\"", username)));

        Set<Role> roles = Optional.ofNullable(user.getRoles()).orElse(new HashSet<>());

        return new AppAuthenticationUser(user, roles);
    }
}
