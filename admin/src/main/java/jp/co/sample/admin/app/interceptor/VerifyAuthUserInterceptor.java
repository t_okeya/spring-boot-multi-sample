package jp.co.sample.admin.app.interceptor;

import jp.co.sample.admin.security.AppAuthentication;
import jp.co.sample.admin.security.AppAuthenticationUser;
import jp.co.sample.shared.domain.entity.Role;
import jp.co.sample.shared.domain.entity.User;
import jp.co.sample.shared.domain.repository.UserRepository;
import jp.co.sample.shared.util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * VerifyAuthUserInterceptor
 * 
 * @author t.okeya
 */
public class VerifyAuthUserInterceptor implements HandlerInterceptor {

    /** {@link AppAuthentication}. */
    @Autowired
    private AppAuthentication auth;

    /** {@link UserRepository}. */
    @Autowired
    private UserRepository userRepository;

    /** {@inheritDoc} */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        // コントローラ処理実施前に行いたい処理を記載する.

        // 静的リソースの場合は不要
        if (handler instanceof ResourceHttpRequestHandler) {
            return true;
        }

        if (this.auth.user().isPresent()) {
            // 認証情報が存在する場合

            // 認証情報から管理ユーザ情報取得
            AppAuthenticationUser authUser = this.auth.user().get();
            User currentUser = authUser.getUser();

            try {
                // ログインIDを元に、管理ユーザ情報再取得
                User user = this.userRepository.findByLogin(currentUser.getLogin()).orElseThrow(()
                        -> new UsernameNotFoundException(String.format("User not found \"%s\"", currentUser.getLogin())));

                // ロール取得
                Set<Role> roles = Optional.ofNullable(user.getRoles()).orElse(new HashSet<>());

                AppAuthenticationUser newAuthUser = new AppAuthenticationUser(user, roles);

                // 認証情報に、管理ユーザ情報を再設定
                Authentication newAuthentication
                        = new UsernamePasswordAuthenticationToken(newAuthUser, null, newAuthUser.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(newAuthentication);

            } catch (UsernameNotFoundException e) {
                LogUtil.error(e.getMessage(), e);
                response.sendRedirect(request.getContextPath() + "/logout");
                return false;
            } catch (PersistenceException e) {
                LogUtil.error("failed unlocked login_id: " + currentUser.getLogin(), e);
                response.sendRedirect(request.getContextPath() + "/logout");
                return false;
            }
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        LogUtil.debug("postHandle");
    }

    /** {@inheritDoc} */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        LogUtil.debug("afterCompletion");
    }

}
