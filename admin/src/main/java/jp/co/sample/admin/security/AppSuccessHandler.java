package jp.co.sample.admin.security;

import jp.co.sample.shared.util.LogUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AppSuccessHandler.
 *
 * @author t.okeya
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class AppSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    /**
     * コンストラクタです.
     * @param defaultTargetUrl .
     */
    public AppSuccessHandler(String defaultTargetUrl) {
        super.setDefaultTargetUrl(defaultTargetUrl);
    }

    /** {@inheritDoc} */
    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request,
                                        final HttpServletResponse response,
                                        final Authentication authentication) throws ServletException, IOException {
        LogUtil.debug("onAuthenticationSuccess");

        AppAuthenticationUser user = (AppAuthenticationUser) authentication.getPrincipal();
        LogUtil.debug("username: " + user.getUsername());
        LogUtil.debug("authentication: " + authentication.isAuthenticated());

        super.onAuthenticationSuccess(request, response, authentication);
    }
}
