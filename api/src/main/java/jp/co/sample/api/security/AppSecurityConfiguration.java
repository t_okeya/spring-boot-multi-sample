package jp.co.sample.api.security;

import jp.co.sample.api.app.controller.ApiController;
import jp.co.sample.api.app.interceptor.AppVerCheckInterceptor;
import jp.co.sample.shared.util.LogUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * AppSecurityConfiguration.
 *
 * @author t.okeya
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@SuppressWarnings({"unused", "WeakerAccess"})
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    /** basename . */
    @Value("${spring.messages.basename}")
    private String basename;
    /** cacheSeconds . */
    @Value("${spring.messages.cache-seconds}")
    private Integer cacheSeconds;
    /** encoding . */
    @Value("${spring.messages.encoding}")
    private String encoding;

    /** {@inheritDoc} */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        LogUtil.debug(this.getClass().getSimpleName() + "configure(HttpSecurity)");

        http
                .csrf().disable() // CSRF対策無効
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // リクエスト毎に認証実施
                .and()

                // 認証・認可の設定
                .authorizeRequests()
                .antMatchers(ApiController.A0001).permitAll() // 認証なしでアクセス可能なURLを設定
                .anyRequest().authenticated() // 上記以外は認証が必要となる設定
                .and()
                // エラー時の処理
                .exceptionHandling().authenticationEntryPoint(this.authenticationEntryPoint());

        http
                // 認証フィルター
                .addFilterBefore(this.preAuthenticatedProcessingFilter(), BasicAuthenticationFilter.class)
                // ヘッダー共通設定
                .headers().addHeaderWriter(new AppAuthenticationHeaderWriter());
    }

    /** {@inheritDoc} */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        LogUtil.debug(this.getClass().getSimpleName() + "configure(AuthenticationManagerBuilder)");

        auth.authenticationProvider(this.preAuthenticatedAuthenticationProvider());
    }

    /** {@inheritDoc} */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.authTokenInterceptor());
    }

    /** {@inheritDoc} */
    @Override
    public Validator getValidator() {
        return this.validator();
    }


    // Bean

    /**
     * @return {@link AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken>}.
     */
    @Bean
    public AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> authenticationUserDetailsService() {
        return new AppAuthenticationUserDetailsService(this.tempAuthTokens());
    }

    /**
     * @return {@link PreAuthenticatedAuthenticationProvider}.
     */
    @Bean
    public PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider() {
        PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
        provider.setPreAuthenticatedUserDetailsService(this.authenticationUserDetailsService());
        provider.setUserDetailsChecker(new AccountStatusUserDetailsChecker());
        return provider;
    }

    /**
     * @return {@link AbstractPreAuthenticatedProcessingFilter}.
     */
    @Bean
    public AbstractPreAuthenticatedProcessingFilter preAuthenticatedProcessingFilter() throws Exception {
        AppPreAuthenticationFilter filter = new AppPreAuthenticationFilter();
        filter.setAuthenticationManager(super.authenticationManager());
        return filter;
    }

    /**
     * @return {@link AppVerCheckInterceptor}.
     */
    @Bean
    public AppVerCheckInterceptor authTokenInterceptor() {
        return new AppVerCheckInterceptor();
    }

    /**
     * @return {@link AppAuthenticationEntryPoint}.
     */
    @Bean
    public AppAuthenticationEntryPoint authenticationEntryPoint() {
        return new AppAuthenticationEntryPoint();
    }

    /**
     * @return {@link LocalValidatorFactoryBean}.
     */
    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(this.messageSource());
        return localValidatorFactoryBean;
    }


    // methods

    /**
     * @return {@link MessageSource}.
     */
    private MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename(this.basename);
        messageSource.setCacheSeconds(this.cacheSeconds);
        messageSource.setDefaultEncoding(this.encoding);
        return messageSource;
    }

    /**
     * @return {@link String[]}.
     */
    private String[] tempAuthTokens() {
        return new String[] {
                ApiController.A0004
        };
    }
}
