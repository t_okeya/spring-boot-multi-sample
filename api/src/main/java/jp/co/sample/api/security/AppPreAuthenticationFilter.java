package jp.co.sample.api.security;

import jp.co.sample.shared.util.LogUtil;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

/**
 * AppPreAuthenticationFilter.
 *
 * @author t.okeya
 */
public class AppPreAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

    /** {@inheritDoc} */
    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        super.doFilter(request, response, chain);
    }

    /** {@inheritDoc} */
    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        LogUtil.debug("getPreAuthenticatedPrincipal");

        // パス
        String resourcePath = new UrlPathHelper().getPathWithinApplication(request);

        // トークン
        Optional<String> token = Optional.ofNullable(request.getHeader("Authorization"));

        String tokenString = "";
        if (token.isPresent() && token.get().startsWith("Bearer ")) {
            tokenString = token.get().substring(7);
        }

        return new AppPreAuthentication(tokenString, resourcePath);
    }

    /** {@inheritDoc} */
    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        LogUtil.debug("getPreAuthenticatedCredentials");

        // nullだと、PreAuthenticatedAuthenticationProvider#authenticateにて、
        // nullチェックに引っかかってしまうため、空文字返却
        return "";
    }

}
