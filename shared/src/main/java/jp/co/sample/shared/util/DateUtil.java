package jp.co.sample.shared.util;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * DateUtil.
 *
 * @author t.okeya
 */
public class DateUtil implements Serializable {

    /**
     * コンストラクタです.
     */
    private DateUtil() {

    }

    /**
     * @return .
     */
    public static DateTime now() {
        return DateTime.now();
    }

    /**
     * @return .
     */
    public static Date nowDate() {
        return now().toDate();
    }

    /**
     * @return .
     */
    public static Timestamp nowTimestamp() {
        Date date = new Date();
        return new Timestamp(date.getTime());
    }

    /**
     * @param date .
     * @param diff .
     * @return .
     */
    public static boolean passedMinutes(Date date, int diff) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMinutes(diff).isBefore(DateTime.now());
    }

    /**
     * @param date .
     * @param diff .
     * @return .
     */
    public static boolean passedHours(Date date, int diff) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusHours(diff).isBefore(DateTime.now());
    }

    /**
     * @param date .
     * @param diff .
     * @return .
     */
    public static boolean passedDays(Date date, int diff) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusDays(diff).isBefore(DateTime.now());
    }

    /**
     * @param startDate .
     * @param endDate .
     * @return .
     */
    public static boolean between(Date startDate, Date endDate) {
        DateTime startDateTime = new DateTime(startDate);
        DateTime endDateTime = new DateTime(endDate);
        return (0 < Days.daysBetween(startDateTime, now()).getDays() && 0 < Days.daysBetween(now(), endDateTime).getDays());
    }

}
