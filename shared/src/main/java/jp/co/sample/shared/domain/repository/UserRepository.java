package jp.co.sample.shared.domain.repository;

import jp.co.sample.shared.domain.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * UserRepository.
 *
 * @author t.okeya
 */
@Repository
@Transactional
public interface UserRepository extends AbstractRepository<User> {

    /**
     * ログインIDで検索.
     *
     * @param login {@link String}
     * @return {@link User}
     */
    Optional<User> findByLogin(String login);
}
