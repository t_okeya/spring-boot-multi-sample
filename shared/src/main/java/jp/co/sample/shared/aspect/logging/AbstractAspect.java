package jp.co.sample.shared.aspect.logging;

import org.aspectj.lang.JoinPoint;

/**
 * 共通織り込み処理の抽象クラスです.
 *
 * @author t.okeya
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractAspect {

    /**
     * 対象メソッドの実施前に実行されます.<br>
     * 対象メソッドの引数を取得しログに出力します.
     *
     * @param point 対象メソッド位置 {@link JoinPoint}
     */
    public abstract void before(JoinPoint point);

    /**
     * 対象メソッドの実施後に実行されます.<br>
     * 対象メソッドの引数を取得しログに出力します.
     *
     * @param point 対象メソッド位置 {@link JoinPoint}
     */
    public abstract void after(JoinPoint point);

    /**
     * 対象メソッド位置 {@link JoinPoint} からメソッドの引数を取得し返却します.
     *
     * @param point 対象メソッド位置 {@link JoinPoint}
     * @return 引数
     */
    protected String getArgs(JoinPoint point) {
        StringBuilder sb = new StringBuilder();
        Object[] objArray = point.getArgs();
        for (Object obj : objArray) {
            sb.append(obj.toString());
            if (objArray[objArray.length - 1] != obj) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }
}
