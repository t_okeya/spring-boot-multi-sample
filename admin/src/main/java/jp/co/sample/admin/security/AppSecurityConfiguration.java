package jp.co.sample.admin.security;

import jp.co.sample.admin.app.interceptor.VerifyAuthUserInterceptor;
import jp.co.sample.shared.util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * AppSecurityConfiguration.
 *
 * @author t.okeya
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@SuppressWarnings({"unused", "WeakerAccess"})
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    /** basename . */
    @Value("${spring.messages.basename}")
    private String[] basename;
    /** cacheSeconds . */
    @Value("${spring.messages.cache-seconds}")
    private Integer cacheSeconds;
    /** encoding . */
    @Value("${spring.messages.encoding}")
    private String encoding;

    /** userDetailsService . */
    @Autowired
    private UserDetailsService userDetailsService;

    /** {@inheritDoc} */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        LogUtil.debug(this.getClass().getSimpleName() + "configure(HttpSecurity)");

        http
                // 認可の設定
                .authorizeRequests()
                .expressionHandler(this.createSecurityExpressionHandler()) // ロールのヒエラルキー設定
                .antMatchers("/").permitAll() // 認証なしでアクセス可能なURLを設定
                .antMatchers("/admin/**").hasAuthority("ROLE_USER") // ロール設定
                .anyRequest().authenticated() // 上記以外は認証が必要となる設定
                .and()

                // ログイン設定
                .formLogin()
                .loginPage("/") // ログイン画面のパス
                .loginProcessingUrl("/login") // 認証処理のパス
                .usernameParameter("login").passwordParameter("password") // ログインID、パスワードのパラメータ設定
                .successHandler(this.successHandler()) // 認証成功時に呼ばれるハンドラクラス
                .failureHandler(this.failureHandler()) // 認証失敗時に呼ばれるハンドラクラス
//                .permitAll()
                .and()

                // ログアウト設定
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // ログアウト処理のパス
                .invalidateHttpSession(true) // セッション情報クリア
                .logoutSuccessUrl("/") // ログアウト完了後のパス
                .deleteCookies("JSESSIONID") // cookieのJSESSIONIDを削除
                .clearAuthentication(true) // 認証情報クリア
//                .permitAll()
                .and()

                // セッション管理
                .sessionManagement()
                .maximumSessions(1) // 1ユーザが許容する最大セッション数
                .sessionRegistry(this.sessionRegistry()).and()
//                .sessionFixation().none()
                .and()

                // CSRFトークン設定
                .csrf().csrfTokenRepository(new CookieCsrfTokenRepository())
        ;
    }

    /** {@inheritDoc} */
    @Override
    public void configure(WebSecurity web) {
        LogUtil.debug(this.getClass().getSimpleName() + "configure(WebSecurity)");

        // 静的リソース(css, js, fonts, img)に対するアクセスはセキュリティ設定を無視する
        web.ignoring().antMatchers(
//                HttpMethod.GET,
                "/webjars/**",
                "/favicon.ico",
                "/bootstrap/**",
                "/dist/**",
                "/plugins/**"
        );
    }

    /** {@inheritDoc} */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        LogUtil.debug(this.getClass().getSimpleName() + "configure(AuthenticationManagerBuilder)");

        auth
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    /** {@inheritDoc} */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.verifyAuthUserInterceptor());
    }

    /** {@inheritDoc} */
    @Override
    public Validator getValidator() {
        return this.validator();
    }


    // Bean

    /**
     * @return {@link VerifyAuthUserInterceptor}
     */
    @Bean
    public VerifyAuthUserInterceptor verifyAuthUserInterceptor() {
        return new VerifyAuthUserInterceptor();
    }

    /**
     * @return {@link PasswordEncoder}
     */
    @Bean
    public PasswordEncoder passwordEncoderBean() {
        return new BCryptPasswordEncoder();
    }

    /**
     * @return {@link AppSuccessHandler}
     */
    @Bean
    public AppSuccessHandler successHandler() {
        return new AppSuccessHandler("/home/");
    }

    /**
     * @return {@link AppFailureHandler}
     */
    @Bean
    public AppFailureHandler failureHandler() {
        return new AppFailureHandler();
    }

    /**
     * @return {@link SessionRegistry}
     */
    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    /**
     * @return {@link AuthenticationManager}.
     * @throws Exception .
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * @return {@link LocalValidatorFactoryBean}.
     */
    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(this.messageSource());
        return localValidatorFactoryBean;
    }


    // methods

    /**
     * @return {@link MessageSource}.
     */
    private MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(this.basename);
        messageSource.setCacheSeconds(this.cacheSeconds);
        messageSource.setDefaultEncoding(this.encoding);
        return messageSource;
    }

    /**
     * @return {@link SecurityExpressionHandler<FilterInvocation>}
     */
    private SecurityExpressionHandler<FilterInvocation> createSecurityExpressionHandler() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_USER");

        DefaultWebSecurityExpressionHandler expressionHandler = new DefaultWebSecurityExpressionHandler();
        expressionHandler.setRoleHierarchy(roleHierarchy);

        return expressionHandler;
    }

}
