package jp.co.sample.api.app.interceptor;

import jp.co.sample.api.security.AppPreAuthentication;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.method.HandlerMethod;

import java.lang.reflect.Method;

/**
 * AbstractInterceptorTest.
 *
 * @author t.okeya
 */
@SuppressWarnings({"WeakerAccess"})
public class AbstractInterceptorTest {

    /** {@link AppPreAuthentication}. */
    @Mock
    protected AppPreAuthentication appPreAuthentication;

    /** {@link HandlerMethod}. */
    protected HandlerMethod handler;

    /** {@link MockHttpServletRequest}. */
    protected MockHttpServletRequest request;

    /** {@link MockHttpServletResponse}. */
    protected MockHttpServletResponse response;

    /**
     * 初期設定を行います.
     */
    protected void setUp() {
        MockitoAnnotations.initMocks(this);

        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
    }

    /**
     * 初期設定を行います.
     * @param method {@link Method}.
     */
    protected void handler(Method method) {

        this.handler = Mockito.mock(HandlerMethod.class);
        Mockito.when(this.handler.getMethod()).thenReturn(method);
    }
}
