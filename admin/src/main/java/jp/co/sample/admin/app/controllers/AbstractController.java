package jp.co.sample.admin.app.controllers;

import org.springframework.beans.BeanUtils;
import org.springframework.web.servlet.ModelAndView;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * AbstractController.
 *
 * @author t.okeya
 */
@SuppressWarnings({"WeakerAccess"})
public abstract class AbstractController {

    /** ページ位置. */
    public static final int PAGE_NUMBER = 0;
    /** 取得件数. */
    public static final int PAGE_SIZE = 5;


    // Method

    /**
     * @param mv {@link ModelAndView}.
     * @param viewName {@link String}.
     * @return {@link ModelAndView}.
     */
    protected ModelAndView response(ModelAndView mv, String viewName) {
        mv.setViewName(viewName);
        return mv;
    }

    /**
     * @param mv {@link ModelAndView}.
     * @param path {@link String}.
     * @return {@link ModelAndView}.
     */
    protected ModelAndView redirect(ModelAndView mv, String path) {
        return this.response(mv, "redirect:" + path);
    }

    /**
     * @param mv {@link ModelAndView}.
     * @param path {@link String}.
     * @return {@link ModelAndView}.
     */
    protected ModelAndView forward(ModelAndView mv, String path) {
        return this.response(mv, "forward:" + path);
    }

    /**
     * @param form {@link Object}
     * @param entity {@link Object}
     * @throws IllegalAccessException .
     * @throws IllegalArgumentException .
     * @throws InvocationTargetException .
     */
    protected void populate(Object form, Object entity)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {

        PropertyDescriptor[] targets = BeanUtils.getPropertyDescriptors(form.getClass());
        for (PropertyDescriptor target : targets) {
            Method readMethod = target.getReadMethod();
            if (readMethod != null) {
                Object value = readMethod.invoke(form);
                if (value != null) {
                    PropertyDescriptor source = BeanUtils.getPropertyDescriptor(entity.getClass(), target.getName());
                    if (source != null) {
                        Method writeMethod = source.getWriteMethod();
                        if (writeMethod != null) {
                            writeMethod.invoke(entity, value);
                        }
                    }
                }

            }
        }
    }
}
