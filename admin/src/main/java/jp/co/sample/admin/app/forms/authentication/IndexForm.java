package jp.co.sample.admin.app.forms.authentication;

import jp.co.sample.admin.app.forms.AbstractForm;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * IndexForm.
 *
 * @author t.okeya
 */
@Getter
@Setter
public class IndexForm extends AbstractForm {

    /** login. */
    @NotBlank(message = "{C0001-0001}")
    private String login;
    /** password. */
    @NotBlank
    private String password;

}
