package jp.co.sample.api.config;

import jp.co.sample.shared.config.LogConfiguration;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * LogConfig.
 *
 * @author t.okeya
 */
@ConfigurationProperties(prefix = "operation-log")
@Data
public class LogConfig implements LogConfiguration {

    /** . */
    private Map<String, String> messages;

    /**
     * @param code .
     * @return .
     */
    public String message(String code) {
        return this.messages.getOrDefault(code, "対象のメッセージコードが存在しません。");
    }

}
