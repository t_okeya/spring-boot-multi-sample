package jp.co.sample.shared.util;

import jp.co.sample.shared.config.LogConfiguration;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * LogUtil.
 *
 * @author t.okeya
 */
@Slf4j
public final class LogUtil {

    /** . */
    private static LogConfiguration logConfiguration;

    /**
     * コンストラクタです.
     */
    private LogUtil() {

    }

    /**
     * @param msg .
     */
    public static void debug(String msg) {
        if (log.isDebugEnabled()) {
            log.debug(msg);
        }
    }

    /**
     * @param msg .
     * @param args .
     */
    public static void debug(String msg, Object... args) {
        if (log.isDebugEnabled()) {
            log.debug(msg, args);
        }
    }

    /**
     * @param code .
     */
    public static void info(String code) {
        log.info(code + " " + config().message(code));
    }

    /**
     * @param code .
     * @param args .
     */
    public static void info(String code, Object... args) {
        log.info(code + " " + config().message(code), args);
    }

    /**
     * @param code .
     */
    public static void warn(String code) {
        log.warn(code + " " + config().message(code));
    }

    /**
     * @param code .
     * @param args .
     */
    public static void warn(String code, Object... args) {
        log.warn(code + " " + config().message(code), args);
    }

    /**
     * @param code .
     */
    public static void error(String code) {
        log.error(code + " " + config().message(code));
    }

    /**
     * @param code .
     * @param args .
     */
    public static void error(String code, Object... args) {
        log.error(code + " " + config().message(code), args);
    }

    /**
     * @param logConfiguration .
     */
    public static void setLogConfiguration(LogConfiguration logConfiguration) {
        LogUtil.logConfiguration = logConfiguration;
    }

    /**
     * @return {@link LogConfiguration}
     */
    private static LogConfiguration config() {
        return Optional.ofNullable(LogUtil.logConfiguration).orElse(new LogConfiguration() {
            @Override
            public Map<String, String> getMessages() { return new LinkedHashMap<>(); }
            @Override
            public String message(String code) { return getMessages().getOrDefault(code, "default message."); }
        });
    }
}
