package jp.co.sample.admin.security;

import jp.co.sample.shared.domain.entity.Role;
import jp.co.sample.shared.domain.entity.User;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * AppAuthenticationUser.
 *
 * @author t.okeya
 */
@Getter
@SuppressWarnings("WeakerAccess")
public class AppAuthenticationUser extends org.springframework.security.core.userdetails.User {

    /** {@link User}. */
    private final User user;

    /**
     * コンストラクタです.
     * @param user {@link User}.
     * @param roles {@link Set<Role>}.
     */
    public AppAuthenticationUser(User user, Set<Role> roles) {
        super(user.getLogin(), user.getPassword(), authorities(roles));
        this.user = user;
    }

    /**
     * @param roles {@link Set<Role>}.
     * @return {@link List<AppGrantedAuthority>}.
     */
    private static List<AppGrantedAuthority> authorities(Set<Role> roles) {
        List<AppGrantedAuthority> authorities = new ArrayList<>();
        roles.forEach(role -> authorities.add(new AppGrantedAuthority(role.getName(), role.getName())));
        return authorities;
    }

//    /**
//     * @param roles {@link Set<Role>}.
//     * @return {@link List<AppGrantedAuthority>}.
//     */
//    private static List<AppGrantedAuthority> authorities(Set<Role> roles) {
//        List<AppGrantedAuthority> authorities = new ArrayList<>();
//        roles.forEach(role ->
//                role.getPermissions().forEach(permission ->
//                        authorities.add(new AppGrantedAuthority(permission.getName(), permission.getName()))));
//        return authorities;
//    }

}
