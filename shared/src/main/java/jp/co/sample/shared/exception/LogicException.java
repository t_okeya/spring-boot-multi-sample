package jp.co.sample.shared.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * LogicException.
 *
 * @author t.okeya
 */
@Getter
@Setter
public class LogicException extends Exception {

    private String code;

    /**
     * コンストラクタです.
     */
    public LogicException() {
        super();
    }

    /**
     * コンストラクタです.
     *
     * @param code コード
     */
    public LogicException(String code) {
        super();
        this.setCode(code);
    }

    /**
     * コンストラクタです.
     *
     * @param code コード
     * @param msg メッセージ
     */
    public LogicException(String code, String msg) {
        super(msg);
        this.setCode(code);
    }

    /**
     * コンストラクタです.
     *
     * @param code コード
     * @param msg メッセージ
     * @param cause 原因
     */
    public LogicException(String code, String msg, Throwable cause) {
        super(msg, cause);
        this.setCode(code);
    }
}
