package jp.co.sample.shared.config;

import java.util.Map;

/**
 * LogConfiguration.
 *
 * @author t.okeya
 */
public interface LogConfiguration {

    /**
     * @return .
     */
    Map<String, String> getMessages();

    /**
     * @param code .
     * @return .
     */
    String message(String code);
}
