package jp.co.sample.api.app.controller;

import jp.co.sample.shared.annotation.AppVerCheck;
import jp.co.sample.shared.util.LogUtil;
import jp.co.sample.shared.util.ResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * UserController.
 *
 * @author t.okeya
 */
@RestController
@SuppressWarnings("unused")
public class UserController extends ApiController {

    /**
     * @param principal {@link Principal}
     * @return {@link ResponseEntity<>}
     */
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<?> user(Principal principal) {

        LogUtil.debug("/api/vi/sample/user");

        return ResponseEntity.ok(ResponseUtil.empty());
    }

    /**
     * @param principal {@link Principal}
     * @return {@link ResponseEntity<>}
     */
    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    @AppVerCheck
    public ResponseEntity<?> admin(Principal principal) {

        LogUtil.debug("/api/vi/sample/admin");

        return ResponseEntity.ok(ResponseUtil.empty());
    }

    /**
     * @return {@link ResponseEntity<>}
     */
    @RequestMapping(value = "/hoge", method = RequestMethod.POST)
    @AppVerCheck
    public ResponseEntity<?> hoge() {

        LogUtil.debug("/api/vi/sample/hoge");

        return ResponseEntity.ok(ResponseUtil.empty());
    }
}
