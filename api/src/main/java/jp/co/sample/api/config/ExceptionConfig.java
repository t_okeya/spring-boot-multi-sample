package jp.co.sample.api.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@ConfigurationProperties(prefix = "exception-log")
@Data
public class ExceptionConfig {

    /** . */
    private Map<String, String> messages;

    /**
     * @param code .
     * @return .
     */
    public String message(String code) {
        return this.messages.getOrDefault(code, "対象のメッセージコードが存在しません。");
    }

}
