package jp.co.sample.admin.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home")
@SuppressWarnings("unused")
public class HomeController extends AbstractController {

    /**
     * ダッシュボード画面表示.
     *
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public ModelAndView index(ModelAndView mv) {

        return super.response(mv, "home/index");
    }
}
