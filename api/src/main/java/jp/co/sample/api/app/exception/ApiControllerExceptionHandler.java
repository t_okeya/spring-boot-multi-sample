package jp.co.sample.api.app.exception;

import jp.co.sample.api.config.ExceptionConfig;
import jp.co.sample.shared.exception.LogicException;
import jp.co.sample.shared.response.error.CommonErrorResponse;
import jp.co.sample.shared.response.error.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ApiControllerExceptionHandler.
 *
 * @author t.okeya
 */
@RestControllerAdvice
public class ApiControllerExceptionHandler extends ResponseEntityExceptionHandler {

    /** ExceptionConfig. */
    @Autowired
    private ExceptionConfig exceptionConfig;

    /**
     * 400 BAD REQUEST.
     * @param ex .
     * @param headers .
     * @param status .
     * @param request .
     * @return .
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        List<ErrorResponse> ers = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(e -> new ErrorResponse(e.getCode(), e.getDefaultMessage(), e.getField()))
                .collect(Collectors.toList());
        return super.handleExceptionInternal(ex, new CommonErrorResponse(ers), headers, status, request);
    }

    /**
     * 403 FORBIDDEN.
     * @param ex .
     * @param request .
     * @return .
     */
    @ExceptionHandler(LogicException.class)
    public ResponseEntity<Object> handleLogicException(LogicException ex, WebRequest request) {
        List<ErrorResponse> errors = new LinkedList<ErrorResponse>() {
            { add(new ErrorResponse(ex.getCode(), exceptionConfig.message(ex.getCode()), null)); }
        };
        HttpStatus status = HttpStatus.FORBIDDEN;
        return super.handleExceptionInternal(ex, new CommonErrorResponse(errors), null, status, request);
    }
}
