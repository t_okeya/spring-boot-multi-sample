package jp.co.sample.api.app.interceptor;

import jp.co.sample.api.app.controller.UserController;
import jp.co.sample.shared.exception.LogicException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Method;

/**
 * AppVerCheckInterceptorTest.
 *
 * @author t.okeya
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AppVerCheckInterceptorTest extends AbstractInterceptorTest {

    /** テスト対象のインターセプタ. */
    @InjectMocks
    private AppVerCheckInterceptor interceptor;

    @Before
    public void setUp() {
        super.setUp();
    }

    /**
     * @throws Exception .
     */
    @Test
    public void case1() throws Exception {
        // @AppVerCheckが設定されているコントローラ
        Method method = UserController.class.getDeclaredMethod("hoge");
        handler(method);

        // リクエストヘッダ
        request.addHeader("X-App-Ver", "1.00.00");

        boolean pass = this.interceptor.preHandle(request, response, handler);

        Assertions.assertThat(pass).isEqualTo(true);
    }

    /**
     * @throws Exception .
     */
    @Test(expected = LogicException.class)
    public void case2() throws Exception {
        // @AppVerCheckが設定されているコントローラ
        Method method = UserController.class.getDeclaredMethod("hoge");
        handler(method);

        // リクエストヘッダ
        request.addHeader("X-App-Ver", "");

        this.interceptor.preHandle(request, response, handler);
    }

    /**
     * @throws Exception .
     */
    @Test(expected = LogicException.class)
    public void case3() throws Exception {
        // @AppVerCheckが設定されているコントローラ
        Method method = UserController.class.getDeclaredMethod("hoge");
        handler(method);

        // リクエストヘッダ
        request.addHeader("App-Ver", "1.00.00");

        this.interceptor.preHandle(request, response, handler);
    }

}