package jp.co.sample.admin.domain.specification;

import jp.co.sample.admin.app.forms.user.SearchForm;
import jp.co.sample.shared.domain.entity.Company;
import jp.co.sample.shared.domain.entity.User;
import jp.co.sample.shared.domain.specification.AbstractSpecification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

/**
 * UserSpecification.
 *
 * @author t.okeya
 */
@Component
public class UserSpecification extends AbstractSpecification<User, SearchForm> {

    @Override
    public Specification<User> getFilter(SearchForm request) {
        return ((root, query, cb) ->
            cb.and(
                    cb.or(
                            this.userAttributeContains("code", request.getUser()).toPredicate(root, query, cb),
                            this.userAttributeContains("name", request.getUser()).toPredicate(root, query, cb)
                    ),
                    cb.or(
                            this.companyAttributeContains("code", request.getCompany()).toPredicate(root, query, cb),
                            this.companyAttributeContains("name", request.getCompany()).toPredicate(root, query, cb)
                    )
            )
        );
    }

    /**
     * @param attribute .
     * @param value .
     * @return .
     */
    private Specification<User> userAttributeContains(String attribute, String value) {
        return (root, query, cb) ->
            cb.like(cb.lower(root.get(attribute)), super.containsLowerCase(value));
    }

    /**
     * @param attribute .
     * @param value .
     * @return .
     */
    private Specification<User> companyAttributeContains(String attribute, String value) {
        return (root, query, cb) -> {
            Join<User, Company> join = root.join("company", JoinType.INNER);
            return cb.like(cb.lower(join.get(attribute)), super.containsLowerCase(value));
        };
    }

}
