package jp.co.sample.shared.util;

import jp.co.sample.shared.util.response.EmptyResponse;

/**
 * ResponseUtil.
 *
 * @author t.okeya
 */
public class ResponseUtil {

    /**
     * コンストラクタです.
     */
    private ResponseUtil() {

    }

    /**
     * @return {@link EmptyResponse}.
     */
    public static EmptyResponse empty() {
        return new EmptyResponse();
    }

}
