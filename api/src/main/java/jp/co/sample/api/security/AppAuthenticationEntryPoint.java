package jp.co.sample.api.security;

import jp.co.sample.shared.util.LogUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AppAuthenticationEntryPoint.
 *
 * @author t.okeya
 */
public class AppAuthenticationEntryPoint implements AuthenticationEntryPoint {

    /** {@inheritDoc} */
    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {
        LogUtil.debug("commence");
        // API用
        httpServletResponse.setContentType("application/json");
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        httpServletResponse.getOutputStream().println("{ \"errors\": [{\"code\": \"001\", \"message\": \"" + e.getMessage() + "\", \"field\": \"consumer\"}]}");
    }
}
