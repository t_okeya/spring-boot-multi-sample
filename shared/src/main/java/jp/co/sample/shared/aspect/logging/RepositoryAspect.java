package jp.co.sample.shared.aspect.logging;

import jp.co.sample.shared.util.LogUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * サービスクラス共通織り込み処理クラスです.<br>
 * jp.co.sample.shared.domain.repository 配下の全クラスの全メソッドを対象とします.<br>
 * jp.co.sample.shared.domain.repository 配下のサブクラスの全クラスの全メソッドを対象とします.
 *
 * @author t.okeya
 */
@Aspect
@Component
@SuppressWarnings("unused")
public class RepositoryAspect extends AbstractAspect {

    /** {@inheritDoc} */
    @Before("execution(* jp.co.sample.shared.domain.repository..*.*(..))")
    @Override
    public void before(JoinPoint point) {
        LogUtil.debug("[Before] Class:" + point.getTarget().getClass().getSimpleName() + ". Method:"
                + point.getSignature().getName() + ". Args:" + super.getArgs(point));
    }

    /** {@inheritDoc} */
    @After("execution(* jp.co.sample.shared.domain.repository..*.*(..))")
    @Override
    public void after(JoinPoint point) {
        LogUtil.debug("[After] Class:" + point.getTarget().getClass().getSimpleName() + ". Method:"
                + point.getSignature().getName() + ".");
    }

}
