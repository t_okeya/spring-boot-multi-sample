package jp.co.sample.shared.domain.specification;

import org.springframework.data.jpa.domain.Specification;

/**
 * AbstractSpecification.
 *
 * @param <T>
 * @param <U>
 *
 * @author t.okeya
 */
public abstract class AbstractSpecification<T, U> {

    /** ワイルドカード文字列. */
    private static final String WILDCARD = "%";

    /**
     * @param request {@link U}.
     * @return {@link Specification<T>}.
     */
    public abstract Specification<T> getFilter(U request);

    /**
     * @param searchField {@link String}.
     * @return {@link String}.
     */
    protected String containsLowerCase(String searchField) {
        if (searchField == null) {
            return WILDCARD;
        }
        return WILDCARD + searchField.toLowerCase() + WILDCARD;
    }
}
