package jp.co.sample.api.app.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ApiController.
 *
 * @author t.okeya
 */
@RequestMapping(value = ApiController.API_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@SuppressWarnings("WeakerAccess")
public abstract class ApiController {

    /** API_PATH. */
    public static final String API_PATH = "/api/v1/sample";

    /** A0001. */
    public static final String A0001 = API_PATH + "/auth";
    /** A0002. */
    public static final String A0002 = API_PATH + "/user";
    /** A0003. */
    public static final String A0003 = API_PATH + "/admin";
    /** A0004. */
    public static final String A0004 = API_PATH + "/hoge";

}
