package jp.co.sample.admin.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * 認証情報インタフェース.
 *
 * @author t.okeya
 */
public interface AppAuthentication {

    /**
     * 認証情報.
     *
     * @return {@link Authentication}.
     */
    Authentication authentication();

    /**
     * 認証しているかどうか.
     *
     * @return {@link Boolean}
     */
    boolean isAuthenticated();

    /**
     * 認証情報.<br>
     *     管理ユーザ情報.<br>
     *     ユーザ区分に応じた関連情報.
     *
     * @return {@link Optional<AppAuthenticationUser>}.
     */
    Optional<AppAuthenticationUser> user();

    /**
     * AppAuthenticationImpl.
     */
    @Component
    class AppAuthenticationImpl implements AppAuthentication {

        /** 匿名ユーザ名. */
        private static final String ANONYMOUS_NAME = "anonymousUser";

        /** {@inheritDoc} */
        @Override
        public Authentication authentication() {
            return SecurityContextHolder.getContext().getAuthentication();
        }

        /** {@inheritDoc} */
        @Override
        public boolean isAuthenticated() {
            return this.authentication() != null && !this.authentication().getPrincipal().equals(ANONYMOUS_NAME);
        }

        /** {@inheritDoc} */
        @Override
        public Optional<AppAuthenticationUser> user() {
            return this.isAuthenticated()
                    ? Optional.of((AppAuthenticationUser) this.authentication().getPrincipal())
                    : Optional.empty();
        }
    }

}
