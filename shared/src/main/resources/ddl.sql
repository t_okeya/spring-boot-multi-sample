DROP TABLE IF EXISTS `user_role`;
DROP TABLE IF EXISTS `role_permission`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `permission`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `company`;


-- Create syntax for TABLE 'company'
CREATE TABLE `company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(255) DEFAULT NULL COMMENT '企業コード',
  `name` varchar(255) NOT NULL COMMENT '企業名',
  `name_kana` varchar(255) NOT NULL COMMENT '企業名カナ',
  `post_code` varchar(8) NOT NULL COMMENT '郵便番号',
  `prefecture` varchar(255) NOT NULL COMMENT '都道府県',
  `address1` varchar(255) NOT NULL COMMENT '住所１',
  `address2` varchar(255) DEFAULT NULL COMMENT '住所２',
  `tel` varchar(13) DEFAULT NULL COMMENT '電話番号',
  `mail` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  `created_user_id` bigint(20) DEFAULT NULL COMMENT '作成ユーザID',
  `created_at` datetime DEFAULT NULL COMMENT '作成日時',
  `updated_user_id` bigint(20) DEFAULT NULL COMMENT '最終更新ユーザID',
  `updated_at` datetime DEFAULT NULL COMMENT '最終更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企業';


-- Create syntax for TABLE 'user'
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_id` bigint(20) NOT NULL COMMENT '企業ID',
  `code` varchar(255) DEFAULT NULL COMMENT 'ユーザコード',
  `login` varchar(255) NOT NULL COMMENT 'ログインID',
  `password` varchar(255) NOT NULL COMMENT 'パスワード',
  `name` varchar(255) NOT NULL COMMENT '氏名',
  `mail` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `last_password_reset_date` datetime DEFAULT NULL COMMENT '最終パスワード設定日',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  `created_user_id` bigint(20) DEFAULT NULL COMMENT '作成ユーザID',
  `created_at` datetime DEFAULT NULL COMMENT '作成日時',
  `updated_user_id` bigint(20) DEFAULT NULL COMMENT '最終更新ユーザID',
  `updated_at` datetime DEFAULT NULL COMMENT '最終更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザ';


-- Create syntax for TABLE 'permission'
CREATE TABLE `permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '権限名',
  `description` varchar(255) DEFAULT NULL COMMENT '説明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='権限';


-- Create syntax for TABLE 'role'
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT 'ロール名',
  `display_order` varchar(255) DEFAULT NULL COMMENT '表示順',
  `description` varchar(255) DEFAULT NULL COMMENT '説明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ロール';


-- Create syntax for TABLE 'role_permission'
CREATE TABLE `role_permission` (
  `permission_id` bigint(20) NOT NULL COMMENT '権限ID',
  `role_id` bigint(20) NOT NULL COMMENT 'ロールID',
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `fk_role_permission_role_id` (`role_id`),
  CONSTRAINT `fk_role_permission_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_role_permission_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='権限ロール';


-- Create syntax for TABLE 'user_role'
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL COMMENT 'ユーザID',
  `role_id` bigint(20) NOT NULL COMMENT 'ロールID',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_role_role_id` (`role_id`),
  CONSTRAINT `fk_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザロール';
