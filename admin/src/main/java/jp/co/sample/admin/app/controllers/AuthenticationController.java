package jp.co.sample.admin.app.controllers;

import jp.co.sample.admin.app.forms.authentication.IndexForm;
import jp.co.sample.shared.util.LogUtil;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 認証管理コントローラ.
 *
 * @author t.okeya
 */
@Controller
@RequestMapping("/")
@SuppressWarnings("unused")
public class AuthenticationController extends AbstractController {

    /**
     * ログイン画面表示.
     *
     * @param mv {@link ModelAndView}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(@ModelAttribute("indexForm") IndexForm form,
                              ModelAndView mv) {
        return super.response(mv, "login");
    }

    /**
     * 認証エラー時.
     *
     * @param form   {@link IndexForm}
     * @param result {@link BindingResult}
     * @param mv     {@link ModelAndView}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView login(@ModelAttribute("indexForm") @Validated IndexForm form,
                              BindingResult result,
                              ModelAndView mv) {

        LogUtil.debug("login validation error");

        if (!result.hasErrors()) {
            LogUtil.debug("result.hasErrors");
        }

        return super.response(mv, "login");
    }


}
