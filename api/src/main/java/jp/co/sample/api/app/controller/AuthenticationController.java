package jp.co.sample.api.app.controller;

import jp.co.sample.api.app.request.authentication.LoginRequest;
import jp.co.sample.api.app.response.authentication.LoginResponse;
import org.joda.time.DateTime;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * AuthenticationController.
 *
 * @author t.okeya
 */
@RestController
@SuppressWarnings("unused")
public class AuthenticationController extends ApiController {

    /**
     * @param request {@link LoginRequest}
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseEntity<?>}
     * @throws AuthenticationException .
     */
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody @Valid LoginRequest request,
            HttpServletResponse response) throws AuthenticationException {

        // レスポンス返却
        return ResponseEntity.ok(new LoginResponse("1234567890", DateTime.now().plusMinutes(30)));
    }
}
