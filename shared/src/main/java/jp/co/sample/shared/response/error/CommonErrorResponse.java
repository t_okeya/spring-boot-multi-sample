package jp.co.sample.shared.response.error;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * CommonErrorResponse.
 *
 * @author t.okeya
 */
@AllArgsConstructor
@Data
public class CommonErrorResponse {

    /** errors. */
    private List<ErrorResponse> errors;
}
