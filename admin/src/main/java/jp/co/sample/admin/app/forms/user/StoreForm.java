package jp.co.sample.admin.app.forms.user;

import jp.co.sample.admin.app.forms.AbstractForm;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * StoreForm.
 *
 * @author t.okeya
 */
@Getter
@Setter
public class StoreForm extends AbstractForm {

    /** code. */
    private String code;
    /** login. */
    @NotBlank
    private String login;
    /** password. */
    @NotBlank
    private String password;
    /** password. */
    @NotBlank
    private String mail;

}
