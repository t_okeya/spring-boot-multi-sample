package jp.co.sample.api.app.response.authentication;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.joda.time.DateTime;

/**
 * LoginResponse.
 *
 * @author t.okeya
 */
@AllArgsConstructor
@Data
public class LoginResponse {

    /** access_token {@link String}. */
    private String access_token;
    /** access_token {@link DateTime}. */
    private DateTime expires_in;

}
