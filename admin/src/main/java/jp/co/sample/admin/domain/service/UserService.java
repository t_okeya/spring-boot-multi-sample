package jp.co.sample.admin.domain.service;

import jp.co.sample.admin.app.forms.user.SearchForm;
import jp.co.sample.shared.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * UserServiceインタフェース.
 *
 * @author t.okeya
 */
public interface UserService {

    /**
     * @param form {@link SearchForm}.
     * @param pageable {@link Pageable}.
     * @return {@link Page<User>}.
     */
    Page<User> findAll(SearchForm form, Pageable pageable);

    /**
     * @param id {@link Long}
     * @return {@link Optional<User>}
     */
    Optional<User> findById(Long id);
}
