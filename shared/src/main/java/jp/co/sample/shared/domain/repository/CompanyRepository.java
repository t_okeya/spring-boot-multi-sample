package jp.co.sample.shared.domain.repository;

import jp.co.sample.shared.domain.entity.Company;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * CompanyRepository.
 *
 * @author t.okeya
 */
@Repository
@Transactional
@SuppressWarnings("unused")
public interface CompanyRepository extends AbstractRepository<Company> {

    /**
     * 企業名で検索.
     *
     * @param name {@link String}
     * @return {@link Company}
     */
    Optional<Company> findByName(String name);
}
