package jp.co.sample.admin.domain.service.impl;

import jp.co.sample.admin.app.forms.user.SearchForm;
import jp.co.sample.admin.domain.service.UserService;
import jp.co.sample.admin.domain.specification.UserSpecification;
import jp.co.sample.shared.domain.entity.User;
import jp.co.sample.shared.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * UserService実処理.
 *
 * @author t.okeya
 */
@Service("userService")
@SuppressWarnings("unused")
public class UserServiceImpl implements UserService {

    /** {@link UserRepository}. */
    @Autowired
    private UserRepository userRepository;
    /** {@link UserSpecification}. */
    @Autowired
    private UserSpecification userSpecification;

    /** {@inheritDoc} */
    @Override
    public Page<User> findAll(SearchForm form, Pageable pageable) {
        Specification<User> specification = this.userSpecification.getFilter(form);
        return this.userRepository.findAll(specification, pageable);
    }

    /** {@inheritDoc} */
    @Override
    public Optional<User> findById(Long id) {
        return this.userRepository.findById(id);
    }

}
