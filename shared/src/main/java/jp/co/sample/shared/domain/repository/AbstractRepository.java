package jp.co.sample.shared.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * リポジトリの基底クラスです.
 *
 * @param <T>
 *
 * @author t.okeya
 */
@NoRepositoryBean
@SuppressWarnings("WeakerAccess")
public interface AbstractRepository<T> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {

}
