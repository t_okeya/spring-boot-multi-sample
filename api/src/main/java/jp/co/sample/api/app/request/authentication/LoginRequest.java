package jp.co.sample.api.app.request.authentication;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * LoginRequest.
 *
 * @author t.okeya
 */
@Data
public class LoginRequest {

    /** ログインID. */
    @NotBlank
    @Size(min = 7, max = 100)
    private String login;

    /** ログインパスワード. */
    @NotBlank
    @Size(min = 7, max = 100)
    private String password;
}
