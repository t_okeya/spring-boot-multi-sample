package jp.co.sample.api.app.interceptor;

import jp.co.sample.shared.annotation.AppVerCheck;
import jp.co.sample.shared.exception.LogicException;
import jp.co.sample.shared.util.LogUtil;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * AppVerCheckInterceptor.
 *
 * @author t.okeya
 */
public class AppVerCheckInterceptor implements HandlerInterceptor {

    /** {@inheritDoc} */
    @Override
    public boolean preHandle(final HttpServletRequest request,
                             final HttpServletResponse response,
                             final Object handler) throws Exception {
        // コントローラ実施前に行いたい処理を記載する.
        LogUtil.debug("preHandle");

        // 静的リソースの場合は不要
        if (handler instanceof ResourceHttpRequestHandler) {
            return true;
        }

        if (AnnotationUtils.findAnnotation(((HandlerMethod) handler).getMethod(), AppVerCheck.class) != null) {

            String str = Optional.ofNullable(request.getHeader("X-App-Ver")).orElse("");

            if (StringUtils.isEmpty(str)) {
                throw new LogicException("001-00001");
            }

            return true;
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void postHandle(final HttpServletRequest request,
                           final HttpServletResponse response,
                           final Object handler, final ModelAndView modelAndView) {
        // コントローラ実施後（レンダリング前）に行いたい処理を記載する.
        LogUtil.debug("postHandle");
    }

    /** {@inheritDoc} */
    @Override
    public void afterCompletion(final HttpServletRequest request,
                                final HttpServletResponse response,
                                final Object handler, final Exception ex) {
        // レンダリング後に行いたい処理を記載する.
        LogUtil.debug("afterCompletion");
    }

}
