package jp.co.sample.admin.app.forms.user;

import jp.co.sample.admin.app.forms.AbstractForm;
import lombok.Getter;
import lombok.Setter;

/**
 * SearchForm.
 *
 * @author t.okeya
 */
@Getter
@Setter
public class SearchForm extends AbstractForm {

    /** company. */
    private String company;
    /** user. */
    private String user;
    /** role. */
    private String role;

}
