package jp.co.sample.shared.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import java.io.IOException;

/**
 * JsonUtil.
 *
 * @author t.okeya
 */
public final class JsonUtil {

    /** . */
    private static final ObjectMapper MAPPER = createMapper();

    /**
     * コンストラクタです.
     */
    private JsonUtil() {

    }

    /**
     * @return .
     */
    private static ObjectMapper createMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(new PropertyNamingStrategy.SnakeCaseStrategy())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    /**
     * @param string .
     * @param clazz .
     * @param <T> .
     * @return .
     */
    public static <T> T parse(String string, Class<T> clazz) {
        try {
            return MAPPER.readValue(string, clazz);
        } catch (IOException e) {
            throw new IllegalArgumentException("The given string value: "
                    + string + " cannot be transformed to Json object");
        }
    }

    /**
     * @param value .
     * @return .
     */
    public static String toString(Object value) {
        try {
            return MAPPER.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("The given Json object value: "
                    + value + " cannot be transformed to a String");
        }
    }

}
