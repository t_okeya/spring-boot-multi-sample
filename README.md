# spring-boot-multi

## コンテナ起動＆停止

```
./gradlew runup
./gradlew rundown
```

## ビルド

```
./gradlew clean && ./gradlew build -x test
```

## データ投入

```
テーブル作成.
$ cat ./shared/src/main/resources/ddl.sql | docker exec -i sample-mysql mysql -u test -ptest sample --default-character-set=utf8

データ投入.
$ cat ./shared/src/main/resources/dml.sql | docker exec -i sample-mysql mysql -u test -ptest sample --default-character-set=utf8
```

```
テーブル全削除.
$ ./gradlew :migrate:flywayClean

テーブル作成、初期データ投入.
$ ./gradlew :migrate:flywayMigrate
```

## 個別ビルド

```
./gradlew clean && ./gradlew :admin:build -x test
./gradlew clean && ./gradlew :api:build -x test
```

## 実行

```
./gradlew :admin:bootRun
./gradlew :api:bootRun
```

## デバッグ

Run/Debug Configurations

```
Gradle project: multi:admin
Tasks: bootRun
VM options: -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5000
```

## login
```
curl -i -X POST \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-H "X-App-OS:iOS" \
-H "X-App-OS-Ver:8.0" \
-H "X-App-OS-Level:80" \
-H "X-App-Ver:Application" \
-d '{"login": "admin", "password": "123"}' \
'http://localhost:8080/api/v1/sample/auth'
```

```
ex)
{
    "access_token": "1234567890"
}
```

## user
```
curl -i -X POST \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-H "X-App-OS:iOS" \
-H "X-App-OS-Ver:8.0" \
-H "X-App-OS-Level:80" \
-H "X-App-Ver:Application" \
-H "Authorization:Bearer 1234567890" \
'http://localhost:8080/api/v1/sample/user'
```

```
ex)
{

}
```

## JUnit
テスト対象のクラス名の上で、Option + Enter を押下し、テストクラスを作成する.
テストは、以下のコマンド実行

```
./gradlew test
./gradlew :admin:test --tests "com.example.HogeTest"
./gradlew :admin:test --tests "com.example.HogeTest.fooMethodTest"
```

## 参考URL

[IntelliJ IDEAショートカット](https://qiita.com/yoppe/items/f7cbeb825c071691d3f2)

[Spring まとめ](https://qiita.com/mikoski01/items/3f5f8793415248eacca6)

[spring-data-jpa アンチパターン](https://qiita.com/sis-yoshiday/items/c29cc8eb3d529f7044ff#%E3%83%95%E3%82%A7%E3%83%83%E3%83%81%E7%AF%84%E5%9B%B2%E3%82%92%E8%A6%8F%E7%A8%8B%E3%81%99%E3%82%8B%E3%82%AF%E3%83%A9%E3%82%B9)

[interceptor](http://kikki.hatenablog.com/entry/2018/02/21/090000)

[validation](http://takeda-san.hatenablog.com/entry/2018/01/18/224244)

[PreAuthentication](https://qiita.com/masatsugumatsus/items/7111983f04c08a5df1f8)
[PreAuthentication Sample](https://github.com/FutureProcessing/spring-boot-security-example)
[Spring Security 使い方メモ　認証・認可](https://qiita.com/opengl-8080/items/032ed0fa27a239bdc1cc)
[独自認証](https://fueiseix.hatenablog.com/entry/2018/03/11/191200)

[spring session](https://sivalabs.in/2018/02/session-management-using-spring-session-jdbc-datastore/)

[RestClient](https://github.com/mouryar/spring-boot-generic-rest-client-sample/blob/master/GenericRestTemplate/src/main/java/com/mourya/sample/GenericRestClient.java)

[json](https://www.javaadvent.com/2017/12/map-json-collections-using-jpa-hibernate.html)

[proxy host](https://qiita.com/uzresk/items/bc7c4a9dc764390cd5ce)

[stored procedure](https://qiita.com/rubytomato@github/items/5de00bc7fb084d026322)

[application.yml 1](https://qiita.com/NagaokaKenichi/items/fd9b5e698776fe9b9cc4)
[application.yml 2](https://github.com/tokuhirom/java-handbook/blob/master/spring/config.md)

[command line batch](https://github.com/muumin/spring-boot-batch-sample)

[junit logger](https://gist.github.com/bloodredsun/a041de13e57bf3c6c040)

[mokito](http://www.baeldung.com/mockito-annotations)


