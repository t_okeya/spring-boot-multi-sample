package jp.co.sample.shared.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

/**
 * 企業テーブル.
 *
 * @author t.okeya
 */
@Entity
@Getter
@Setter
@Table(name = "company")
@Where(clause = "deleted_at IS NULL") // 論理削除されてない条件
public class Company extends AbstractEntity {

    // Column

    /**
     * ID
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 企業コード.
     */
    @Column(name = "code")
    private String code;

    /**
     * 企業名.
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * 企業名カナ.
     */
    @Column(name = "name_kana", nullable = false)
    private String name_kana;

    /**
     * 郵便番号.
     */
    @Column(name = "post_code", nullable = false)
    private String postCode;

    /**
     * 都道府県.
     */
    @Column(name = "prefecture", nullable = false)
    private String prefecture;

    /**
     * 住所１.
     */
    @Column(name = "address1", nullable = false)
    private String address1;

    /**
     * 住所２.
     */
    @Column(name = "address2")
    private String address2;

    /**
     * 電話番号.
     */
    @Column(name = "tel")
    private String tel;

    /**
     * メールアドレス.
     */
    @Column(name = "mail")
    private String mail;

    /**
     * 削除日時.
     */
    @Column(name = "deleted_at")
    private Timestamp deletedAt;

    /**
     * 作成ユーザID.
     */
    @Column(name = "created_user_id", insertable = false, updatable = false)
    private Long createdUserId;

    /**
     * 作成日時.
     */
    @Column(name = "created_at", insertable = false, updatable = false)
    private Timestamp createdAt;

    /**
     * 最終更新ユーザID.
     */
    @Column(name = "updated_user_id", insertable = false, updatable = false)
    private Long updatedUserId;

    /**
     * 最終更新日時.
     */
    @Column(name = "updated_at", insertable = false, updatable = false)
    private Timestamp updatedAt;


    // Relation

    @JsonIgnore
    @OneToMany(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "company")
    private Set<User> users;


    // Method


}
