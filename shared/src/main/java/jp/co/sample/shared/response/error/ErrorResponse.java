package jp.co.sample.shared.response.error;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * ErrorResponse.
 *
 * @author t.okeya
 */
@AllArgsConstructor
@Data
public class ErrorResponse {

    /** code. */
    private String code;
    /** message. */
    private String message;
    /** field. */
    private String field;
}
