package jp.co.sample.admin.security;

import jp.co.sample.shared.util.LogUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * AppFailureHandler.
 *
 * @author t.okeya
 */
public class AppFailureHandler implements AuthenticationFailureHandler {

    /** {@inheritDoc} */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        LogUtil.debug("onAuthenticationFailure");
        request.getRequestDispatcher("/").forward(request, response);
    }
}
